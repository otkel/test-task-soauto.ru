@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Chats</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if($users)
                            <div class="row" id="all-users">
                                @foreach($users as $user)
                                    @include('parts.loop-items.user-message-group',['user' => $user])
                                @endforeach
                            </div>
                        @else
                            Sorry, here anyone else of you :(
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
