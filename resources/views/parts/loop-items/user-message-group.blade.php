<div class="col-sm-12">
    <div class="row">
        <div class="col-sm-3 text-center">
            <p>{{ $user->name }}</p>
        </div>
        <div class="col-sm-6 text-center">
            <p>{{ Auth::user()->lastMessage($user->id) }}</p>
        </div>
        <div class="col-sm-3 text-center">
            <a href="{{ url("/chat/$user->id") }}" class="btn btn-primary">
                go to chat
            </a>
        </div>
    </div>
</div>