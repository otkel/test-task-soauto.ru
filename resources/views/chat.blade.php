@extends('layouts.app')

@section('content')
    <div class="container-fluid" id="chat-window">
        <div class="row chat-title-wrapper">
            <div class="col-md-12 text-center">
                <h3>Conversation with {{ $user->name }}</h3>
            </div>
        </div>
        <div class="container mt-4" id="chat-messages">
            @php
                $me = Auth::user();
                $messages = $me->messages_with($user->id)
            @endphp
            @foreach($messages as $message)
                @php($is_my = $message->from_id === $me->id)
                <div class="row mt-1">
                    <div class="col-sm-6 col-xs-12 {{ $is_my ? "my-message offset-sm-6" : "others-message" }}">
                        <div class="card">
                            <div class="card-header  d-flex justify-content-between">
                                <p> {{ $is_my ? $me->name : $user->name }} </p>
                                <p>{{ $message->created_at }}</p>
                            </div>
                            <div class="card-body"><p>{{ $message->content }}</p></div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="container" id="type-message">
            <div class="row">
                <div class="col-sm-10">
                    <textarea name="new-message" class="form-control" id="new-message" cols="30"
                              rows="5" placeholder="New message..."></textarea>
                </div>
                <div class="col-sm-2 text-center">
                    <button class="btn btn-primary" id="send-message">
                        Send
                    </button>
                </div>
            </div>

        </div>

    </div>
@endsection
@section('scripts')
    <script>
        let lastMessageDateTime = "{{ $messages->last()->created_at ?? '' }}";
        let my_id = {{ Auth::id() }};
        let user_id = {{ $user->id }};
        $(document).ready(function () {
            let $chatWindow = $('#chat-messages');
            let $newMessageWindow = $('#new-message');
            setInterval(function () {
                $.get(`/chat/{{$user->id}}/newMessages/${encodeURIComponent(lastMessageDateTime)}`, function (data) {
                    console.log(data);
                    if (data['messages'].length) {
                        data['messages'].forEach(function (item) {
                            let is_me = item['from_id'] === my_id;
                            let message = `
                            <div class="row mt-1">
                                <div class="col-sm-6 col-xs-12 ${is_me ? "my-message offset-sm-6" : "others-message"} ">
                        <div class="card">
                            <div class="card-header  d-flex justify-content-between">
                                <p> ${is_me ? "{{$me->name}}" : "{{$user->name}}"} </p>
                                <p>${item['created_at']}</p>
                            </div>
                            <div class="card-body"><p>${item['content']}</p></div>
                        </div>
                    </div>
                </div>`;
                            $chatWindow.append(message);
                        });
                        lastMessageDateTime = data['messages'][data['messages'].length - 1]['created_at'];
                    }
                });
            }, 3000);
            $.ajaxSetup({
                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                }
            });
            $('#send-message').click(function () {

                $.ajax({
                    type: 'POST',
                    url: '/chat/{{$user->id}}/newMessage',
                    data: {message: $newMessageWindow.val()},
                    success: function (data) {
                        if (data === "error")
                            return;
                        lastMessageDateTime = data['created_at'];
                        let message = `
                            <div class="row mt-1">
                                <div class="col-sm-6 col-xs-12 my-message offset-sm-6 ">
                        <div class="card">
                            <div class="card-header  d-flex justify-content-between">
                                <p> {{$me->name}} </p>
                                <p>${data['created_at']}</p>
                            </div>
                            <div class="card-body"><p>${data['content']}</p></div>
                        </div>
                    </div>
                </div>`;
                        $chatWindow.append(message);
                        $newMessageWindow.val('');
                        $chatWindow.scrollTop($chatWindow[0].scrollHeight);

                    }
                });
            })
        })
    </script>
@endsection