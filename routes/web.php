<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Auth::routes(/*['verify' => true]*/);

Route::get('/', 'HomeController@index')->name('home');
Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => 'chat/{user_id}'], function () {
        Route::get('/', 'ChatController@chat');
        Route::get('/newMessages/{lastMessageDateTime}', 'ChatController@newMessages');
        Route::post('/newMessage', 'ChatController@newMessage');
    });


});




