<?php

namespace App\Http\Controllers;

use App\Message;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{
    public function chat($user_id)
    {
        $user = User::find($user_id);
        return view('chat', compact('user'));
    }

    public function newMessages($user_id, $lastMessageDateTime)
    {

        $messages = Auth::user()->messages_with($user_id, true)->where('created_at', '>', $lastMessageDateTime)->get();
        return response()->json([
            'messages' => $messages
        ]);

    }

    public function newMessage($user_id, Request $request)
    {
        if ($request->message) {
            $message = new Message([
                'to_id' => $user_id,
                'from_id' => Auth::id(),
                'content' => $request->message,
            ]);
            $message->save();
            return response()->json($message);
        }
        return 'error';
    }
}
