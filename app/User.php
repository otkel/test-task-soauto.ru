<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable //implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function lastMessage($user_id)
    {
        $user = User::find($user_id);
        if (!$user) {
            return "Doesn't exist user with this id $user_id";
        }
        $last_message = $this->messages_with($user_id)->last();
        if ($last_message) {
            return $last_message->from_id === Auth::id() ? "You: $last_message->content" : "$user->name: $last_message->content";
        } else {
            return "Conversation not started yet!";
        }


    }

    public function messages_with($user_id, $return_query = false)
    {
        $user = User::find($user_id);
        if (!$user) {
            return "Doesn't exist user with this id $user_id";
        }
        $messages = Message::where(function ($query) use ($user_id) {
            $query->where('from_id', Auth::id());
            $query->where('to_id', $user_id);
            $query->orWhere(function ($query) use ($user_id) {
                $query->where('from_id', $user_id);
                $query->where('to_id', Auth::id());
            });
        });
        if (!$return_query) {
            $messages = $messages->get();
        }
        return $messages;
    }
}
